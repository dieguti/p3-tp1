package interfaz;

import juego.Tablero;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.JPanel;

public class Interfaz {

	private JFrame ventana;
	private JTextArea cuadroDeMsj;
	private JPanel contenedorDeBaldosas;
	private Tablero tableroDeValores;
	private JTextField [][] cuadros;
	private Color baldosa;
	private int cuadrosTamanio;
	private int cuadrosPosX;
	private int cuadrosPosY;
	private JTextField cuadroPuntaje;
	private String puntajeAMostrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		ventanaPrincipal();
		inicio();
		tableroDeJuego();
		puntaje();
		actualizarPuntaje();

	}

	private void ventanaPrincipal() {
		ventana = new JFrame();
		ventana.getContentPane().setBackground(new Color(0xFAF8EF));
		ventana.setTitle("2048");
		ventana.setBounds(100, 100, 540, 500);
		ventana.setResizable(false);
		ventana.getContentPane().setLayout(null);

		// Manejo del cierre de Ventana
		ventana.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		ventana.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});

		// Titulo
		JLabel titulo = new JLabel("2048");
		titulo.setBounds(120, 11, 158, 50);
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setFont(new Font("Arial", Font.BOLD, 50));
		titulo.setForeground(new Color(143, 122, 104));
		ventana.getContentPane().add(titulo);

	}

	private void close() {
		System.exit(0);
	}

	public void inicio() {
		cuadroDeMsjAlUsuario();
		setMsj("Suma 2048");
		tableroDeValores = new Tablero();
	}

	public void tableroDeJuego() {
		// Crea el panel de las baldosas
		contenedorDeBaldosas = new JPanel();
		contenedorDeBaldosas.setBackground(new Color(187, 173, 160));
		contenedorDeBaldosas.setBounds(56, 71, 290, 290);
		ventana.getContentPane().add(contenedorDeBaldosas);
		contenedorDeBaldosas.setLayout(null);

		//se escucha eventos de teclado del panel
		escucharTeclado(contenedorDeBaldosas,null);

		int tamanio = 4;

        //Inicializan variables del tablero gráfico y la posicion de las baldosas
		cuadros = new JTextField[tamanio][tamanio];
		cuadrosTamanio = 60;
		cuadrosPosX = 10;
		cuadrosPosY = 10;
		int tamanioDeCuadro = 70;
		int posicionYInicial = 10;

		// se crean los cuadros, se acomodan las baldosas y se los acomoda en el panel

		for (int i = 0; i< tamanio; i++){
			for (int j = 0; j < tamanio; j++){

				if(j != 0 && j < tamanio){
					cuadros [i][j] = new JTextField();
					cuadros[i][j].setBorder(null);
					cuadros[i][j].setBackground(new Color(0xCDC1B4));
					cuadros[i][j].setFont(new Font("Comic Sans MS", Font.BOLD ,25));
					cuadros[i][j].setHorizontalAlignment(SwingConstants.CENTER);
					cuadros[i][j].setEditable(false);
					cuadros[i][j].setFocusable(false);
					cuadros[i][j].setBounds(cuadrosPosY,cuadrosPosY,cuadrosTamanio,cuadrosTamanio);
					contenedorDeBaldosas.add(cuadros[i][j]);
					cuadros[i][j].setColumns(10);

					if(j == tamanio - 1){
						cuadrosPosX += tamanioDeCuadro;
						cuadrosPosY = posicionYInicial;
					}

				}
			}
		}

		actualizarTableroGrafico();

	}

	private void actualizarTableroGrafico() {
		for (int i = 0; i < 4; i ++){
			for (int j = 0; j < 4; j++){
				//Se obtienen los valores del tablero de lógica de negocio
				String valor = Integer.toString(tableroDeValores.getValor(i,j));
				actualizarCuadros(cuadros[i][j], valor);
			}
		}
	}
	// Este método actualiza la vista grafica del tabelro (baldosas) con sus respectivos valores y colores
	private void actualizarCuadros(JTextField cuadro, String valor) {
		if(!valor.equals("0")){
			cuadro.setText(valor);

			// De acuerdo al valor que posee el cuadro, le da un color de texto y de fondo

			if (cuadro.getText().equals("2")){
				cuadro.setBackground(baldosa = new Baldosa(2).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("4")){
				cuadro.setBackground(baldosa = new Baldosa(4).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("8")){
				cuadro.setBackground(baldosa = new Baldosa(8).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("16")){
				cuadro.setBackground(baldosa = new Baldosa(16).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("32")){
				cuadro.setBackground(baldosa = new Baldosa(32).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("64")){
				cuadro.setBackground(baldosa = new Baldosa(64).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("128")){
				cuadro.setBackground(baldosa = new Baldosa(128).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("256")){
				cuadro.setBackground(baldosa = new Baldosa(256).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("512")){
				cuadro.setBackground(baldosa = new Baldosa(512).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("1024")){
				cuadro.setBackground(baldosa = new Baldosa(1024).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
			if (cuadro.getText().equals("2048")){
				cuadro.setBackground(baldosa = new Baldosa(2048).obtenerColorBaldosa());
				cuadro.setForeground(new Color (119,110,101));
				cuadro.setFont(new Font("Comic Sans MS", Font.BOLD,25));
			}
		}
		// Si es cero
	/*	else{
			cuadro.setText(null);
			cuadro.setBackground(baldosa = new Baldosa (0).obtenerColorBaldosa());
		}*/
	}

	public void escucharTeclado(JPanel panel, JButton boton){
		Object o = new Object();
		if (panel != null){
			o = panel;
			((Component) o).setFocusable(true);
		}
		else {
			o= boton;
			((Component)o).setFocusable(false);
		}

		((Component) o).addKeyListener(new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				controlDeMoviemiento(e);

				if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
					System.exit(0);
				}

			}
		});

	}

	public void controlDeMoviemiento(KeyEvent e){
		String tecla= "";
		int codigoTecla = 0;
		if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			tecla= "derecha";
			codigoTecla = KeyEvent.VK_RIGHT;
			System.out.println("derecha");
		}
		if(e.getKeyCode()==KeyEvent.VK_LEFT) {
			tecla="izquierda";
			codigoTecla = KeyEvent.VK_LEFT;
			System.out.println("izquierda");
		}
		if(e.getKeyCode()==KeyEvent.VK_UP) {
			tecla="arriba";
			codigoTecla = KeyEvent.VK_UP;
			System.out.println("arriba");
		}
		if(e.getKeyCode()==KeyEvent.VK_DOWN) {
			tecla="abajo";
			codigoTecla = KeyEvent.VK_DOWN;
			System.out.println("abajo");
		}



	}

	public void puntaje(){
		cuadroPuntaje = new JTextField();
		cuadroPuntaje.setFont(new Font("Comic Sans MS", Font.BOLD,25));
		cuadroPuntaje.setHorizontalAlignment(SwingConstants.RIGHT);
		cuadroPuntaje.setFocusable(false);
		cuadroPuntaje.setEditable(false);
		cuadroPuntaje.setBorder(null);
		cuadroPuntaje.setBackground(new Color(187,173,160));
		cuadroPuntaje.setForeground(Color.white);
		cuadroPuntaje.setBounds(400,71,90,50);
		ventana.getContentPane().add(cuadroPuntaje);

		JLabel labelPuntaje = new JLabel("Puntaje");
		labelPuntaje.setForeground(new Color(143,122,102));
		labelPuntaje.setFont(new Font("Comic Sans MS",Font.BOLD,20));
		labelPuntaje.setHorizontalAlignment(SwingConstants.CENTER);
		labelPuntaje.setBounds(400,46,90,23);
		ventana.getContentPane().add(labelPuntaje);
	}

	public void actualizarPuntaje(){
		puntajeAMostrar = Integer.toString(tableroDeValores.getPuntaje());
		cuadroPuntaje.setText(puntajeAMostrar);
	}

	public void setMsj(String msj) {
		cuadroDeMsj.selectAll();
		cuadroDeMsj.replaceSelection(msj);
	}

	public void cuadroDeMsjAlUsuario() {

		cuadroDeMsj = new JTextArea(3, 3);// numero de filas y columnas
		cuadroDeMsj.setLineWrap(true);
		cuadroDeMsj.setWrapStyleWord(true);// para que haga salto de linea al final sin cortar palabras
		cuadroDeMsj.setBounds(400, 130, 90, 20);
		cuadroDeMsj.setBackground(new Color(0x000000));
		cuadroDeMsj.setForeground(Color.white);
		cuadroDeMsj.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		cuadroDeMsj.setFocusable(false);
		cuadroDeMsj.setBorder(null);
		cuadroDeMsj.setMargin(new Insets(200, 500, 200, 200));

		ventana.getContentPane().add(cuadroDeMsj);
	}
}
