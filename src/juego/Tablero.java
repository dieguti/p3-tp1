package juego;

import java.util.Random;

public class Tablero {

	public static final int FILAS = 4;
	public static final int COLUMNAS = 4;
	private int tamanio = 4;
	
	private int[][] tablero;



	private int puntaje=0;
	
	public Tablero() {
		tablero = new int [FILAS][COLUMNAS];
		for (int fila=0; fila<FILAS; fila++) {
			for (int col=0; col<COLUMNAS; col++) {
				tablero[fila][col] = 0;
			}
		}
		
		ponerDosOCuatro();
		ponerDosOCuatro();
	}
	
	//se termina la partida si el jugador llego a 2048 o se quedo sin espacios
	public boolean finPartida() {
		return ganador() || vacias()==0;
	}
	
	//revisa si el jugador llego a 2048
	public boolean ganador() {
		for (int fila=0; fila<FILAS; fila++)
			for (int col=0; col<COLUMNAS; col++)
				if(tablero[fila][col] == 2048)
					return true;
		return false;
	}
	
	//cuenta la cantidad de 0s en el tablero
	public int vacias() {
		int cont=0;
		for (int fila=0; fila<FILAS; fila++) {
			for (int col=0; col<COLUMNAS; col++) {
				if(tablero[fila][col]==0)
					cont++;
			}
		}
		return cont;
	}
	
	//agrega un 2 o 4 aleatoriamente en el tablero
	private void ponerDosOCuatro() {
		int fila;
		int col;
		
		Random random = new Random();
		
		do {
			fila = random.nextInt(FILAS);
		} while(vaciasEnFila(fila)==0);
		do {
			col = random.nextInt(COLUMNAS);
		} while(tablero[fila][col]!=0);
		
		tablero[fila][col]= elegirNumero();
	}
	
	private int elegirNumero() {
        Random random = new Random();
        double probabilidad = random.nextDouble(); // Genera un número aleatorio entre 0 y 1

        if (probabilidad < 0.85) { // 85% de probabilidad
            return 2;
        } else {
            return 4;
        }
    }
	
	//cuenta los 0s en una fila
	private int vaciasEnFila(int fila) {
		int cont = 0;
		for (int col=0; col<COLUMNAS; col++) {
			if (tablero[fila][col]==0)
				cont++;
		}
		return cont;
	}
	
	//cuenta los 0s en una columna
	private int vaciasEnColumna(int col) {
		int cont = 0;
		for (int fila=0; fila<FILAS; fila++) {
			if (tablero[fila][col]==0)
				cont++;
		}
		return cont;
	}
	///////////////////////////////////////////////////Movimientos//////////////////////////////////////////////
	//movimiento hacia arriba
	public void moverArriba() {
		for(int col=0; col<COLUMNAS; col++) {
			moverArribaPorColumna(col);
			sumarArriba(col);
			moverArribaPorColumna(col);
		}
		if(!finPartida())
			ponerDosOCuatro();
	}
	
	private void moverArribaPorColumna(int col) {
		if (vaciasEnColumna(col)<COLUMNAS) {
			for (int veces=0; veces<COLUMNAS-1; veces++) {
				for (int fila=0; fila<FILAS-1; fila++) {
					if (tablero[fila][col]==0) {
						tablero[fila][col] = tablero[fila+1][col];
						tablero[fila+1][col] = 0;
					}
				}
			}
		}
	}
	
	private void sumarArriba(int col) {
		if (vaciasEnColumna(col)>=2) {
			for (int fila=0; fila<FILAS-1; fila++) {
				if (tablero[fila][col] == tablero[fila+1][col])
					tablero[fila][col] *= 2;
					//puntaje += tablero[fila][col];
					tablero[fila+1][col] = 0;
			}
		}			
	}
	
	//movimiento hacia abajo
	public void moverAbajo() {
		for(int col=0; col<COLUMNAS; col++) {
			moverAbajoPorColumna(col);
			sumarAbajo(col);
			moverAbajoPorColumna(col);
		}
		if(!finPartida())
			ponerDosOCuatro();
	}
	
	private void moverAbajoPorColumna(int col) {
		if (vaciasEnColumna(col)<COLUMNAS) {
			for (int veces=0; veces<COLUMNAS-1; veces++) {
				for (int fila=FILAS-1; fila>0; fila--) {
					if (tablero[fila][col]==0) {
						tablero[fila][col] = tablero[fila-1][col];
						tablero[fila-1][col] = 0;
					}
				}
			}
		}
	}
	
	private void sumarAbajo(int col) {
		if (vaciasEnColumna(col)>=2) {
			for (int fila=FILAS-1; fila<0; fila--) {
				if (tablero[fila][col] == tablero[fila-1][col])
					tablero[fila][col] *= 2;
					tablero[fila-1][col] = 0;
			}
		}			
	}
	
	//movimiento hacia derecha
	public void moverDerecha() {
		for(int fila=0; fila<FILAS; fila++) {
			moverDerechaPorFila(fila);
			sumarDerecha(fila);
			moverDerechaPorFila(fila);
		}
		if(!finPartida())
			ponerDosOCuatro();
	}
	
	private void moverDerechaPorFila(int fila) {
		if (vaciasEnFila(fila)<FILAS) {
			for (int veces=0; veces<FILAS-1; veces++) {
				for (int col=COLUMNAS-1; col>0; col--) {
					if (tablero[fila][col]==0) {
						tablero[fila][col] = tablero[fila][col-1];
						tablero[fila][col-1] = 0;
					}
				}
			}
		}
	}
	
	private void sumarDerecha(int fila) {
		if (vaciasEnFila(fila)>=2) {
			for (int col=COLUMNAS-1; col>0; col--) {
				if (tablero[fila][col] == tablero[fila][col-1])
					tablero[fila][col] *= 2;
					tablero[fila][col-1] = 0;
			}
		}			
	}
	
	//movimiento hacia izquierda
	public void moverIzquierda() {
		for(int fila=0; fila<FILAS; fila++) {
			moverIzquierdaPorFila(fila);
			sumarIzquierda(fila);
			moverIzquierdaPorFila(fila);
		}
		if(!finPartida())
			ponerDosOCuatro();
	}
	
	private void moverIzquierdaPorFila(int fila) {
		if (vaciasEnFila(fila)<FILAS) {
			for (int veces=0; veces<FILAS-1; veces++) {
				for (int col=0; col<COLUMNAS-1; col++) {
					if (tablero[fila][col]==0) {
						tablero[fila][col] = tablero[fila][col+1];
						tablero[fila][col+1] = 0;
					}
				}
			}
		}
	}
	
	private void sumarIzquierda(int fila) {
		if (vaciasEnFila(fila)>=2) {
			for (int col=0; col<COLUMNAS-1; col++) {
				if (tablero[fila][col] == tablero[fila][col+1])
					tablero[fila][col] *= 2;
					tablero[fila][col+1] = 0;
			}
		}			
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public int getTamanio(){return tamanio;}
	public int getValor (int fila, int columna){return tablero[fila][columna];}
	public void  setValor(int fila, int columna, int valor){tablero[fila][columna] = valor; }
	public int getPuntaje() {return puntaje;}
	
}
